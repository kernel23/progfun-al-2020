//package example
//
//import entities.Direction
//import org.junit.Test
//import org.junit.Assert._
///**
// * Owners:
// * SAIDI Amina
// * OUADOUD Ebrahim
// * FAHRAOUI Zakaria
// */
//class DirectionTest {
//
//  /**
//   * Test parse method
//   */
//  @Test
//  def testParse(): Unit = {
//    assertEquals(Direction.parse('N'), Direction.North)
//    assertEquals(Direction.parse('E'), Direction.East)
//    assertEquals(Direction.parse('W'), Direction.West)
//    assertEquals(Direction.parse('S'), Direction.South)
//  }
//
//}
