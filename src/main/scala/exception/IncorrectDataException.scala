package exception

/**
 * Owners:
 * SAIDI Amina
 * OUADOUD Ebrahim
 * FAHRAOUI Zakaria
 */
case class IncorrectDataException(MessageError: String)
    extends Exception(MessageError) {}
