package entities

import entities.Mowers.Mower
import exception.IncorrectDataException

import scala.annotation.tailrec

/**
 * Owners:
 * SAIDI Amina
 * OUADOUD Ebrahim
 * FAHRAOUI Zakaria
 */
object MowerCommand {
  def changeDirection(
      mower: Mower,
      rotationInstruction: Instructions.Value
  ): Mower =
    rotationInstruction match {
      case Instructions.Right =>
        mower.direction match {
          case Direction.North => mower._direction.update(Direction.East)(mower)
          case Direction.East  => mower._direction.update(Direction.South)(mower)
          case Direction.South => mower._direction.update(Direction.West)(mower)
          case Direction.West  => mower._direction.update(Direction.North)(mower)
        }
      case Instructions.Left =>
        mower.direction match {
          case Direction.North => mower._direction.update(Direction.West)(mower)
          case Direction.West  => mower._direction.update(Direction.South)(mower)
          case Direction.South => mower._direction.update(Direction.East)(mower)
          case Direction.East  => mower._direction.update(Direction.North)(mower)
        }
      case _ => throw IncorrectDataException("Directions we don't exist")
    }

  def updateX(mower: Mower, xCoordinate: Int): Mower =
    mower.copy(
      point = mower.point.copy(x = xCoordinate)
    )

  def updateY(mower: Mower, yCoordinate: Int): Mower =
    mower.copy(
      point = mower.point.copy(y = yCoordinate)
    )

  def updateDirection(mower: Mower, direction: Direction.Value): Mower =
    mower.copy(
      direction = direction
    )

  def getDirection(mower: Mower): Mower =
    mower.direction match {
      case Direction.North =>
        val updatedMower = updateY(mower, mower.point.y + 1)
        updatedMower
      case Direction.East =>
        val updatedMower = updateX(mower, mower.point.x + 1)
        updatedMower
      case Direction.South =>
        val updatedMower = updateY(mower, mower.point.y - 1)
        updatedMower
      case Direction.West =>
        val updatedMower = updateX(mower, mower.point.x - 1)
        updatedMower
      case _ => throw IncorrectDataException("Directions we don't exist")
    }

  def forward(mower: Mower, instruction: Instructions.Value): Mower =
    instruction match {
      case Instructions.Forward => getDirection(mower)
      case _                    => throw IncorrectDataException("Blocked for Forward")
    }

  def executeInstruction(mower: Mower, instruction: Instructions.Value): Mower =
    instruction match {
      case Instructions.Right   => changeDirection(mower, instruction)
      case Instructions.Left    => changeDirection(mower, instruction)
      case Instructions.Forward => forward(mower, instruction)
      case _                    => throw IncorrectDataException("Instructions we don't exist")

    }

  @tailrec
  def executeInstructionList(
      mower: Mower,
      instructions: List[Instructions.Value]
  ): Mower =
    instructions match {
      case Nil => mower
      case head :: tail =>
        executeInstructionList(executeInstruction(mower, head), tail)

    }

  def executeHelper(
      mowers: Map[Mower, List[Instructions.Value]]
  ): List[MowerState] =
    mowers.keys
      .map(m =>
        new MowerState(
          m,
          mowers(m),
          executeInstructionList(m, mowers(m))
        )
      )
      .toList

  def execute(
      mowers: Map[Mower, List[Instructions.Value]]
  ): List[MowerState] = {
    executeHelper(mowers)
  }

}
