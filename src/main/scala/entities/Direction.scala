package entities

import exception.IncorrectDataException

/**
 * * Owners:
 * SAIDI Amina
 * OUADOUD Ebrahim
 * FAHRAOUI Zakaria
 *
 * Enumeration représentant les directions disponibles
 * L'ordre est important : Voir la fonction "getDirection" sur l'entite MowerCommand
 * 		| Left(Direction(i)) = Direction((i - 1) % nbDirections)
 * 		| Right(Direction(i)) = Direction((i + 1) % nbDirections)
 * Donc on peut facilement ajouter new directions (par exemple NE, entre N et E)
 */
object Direction extends Enumeration {
  val North, East, West, South = Value

  @throws(classOf[IncorrectDataException])
  def parse(letter: Char): Direction.Value =
    letter match {
      case 'N' => North
      case 'E' => East
      case 'W' => West
      case 'S' => South
      case _   => throw IncorrectDataException("Directions we don't exist")
    }

}
