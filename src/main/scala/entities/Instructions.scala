package entities

/**
 * Owners:
 * SAIDI Amina
 * OUADOUD Ebrahim
 * FAHRAOUI Zakaria
 *
 *  Available commands for a Mower
 */
import exception.IncorrectDataException

object Instructions extends Enumeration {
  val Left, Right, Forward = Value

  def parse(letter: Char): Instructions.Value =
    letter match {
      case 'G' => Left
      case 'D' => Right
      case 'A' => Forward
      case _ =>
        throw IncorrectDataException("instructions we don't exist")
    }

}
