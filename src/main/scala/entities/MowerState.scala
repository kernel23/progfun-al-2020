package entities

import entities.Mowers.Mower
import play.api.libs.json.{Json, Writes}

/**@param start,
 * @param instructions,
 * @param end,
 * Owners :
 * SAIDI Amina
 * OUADOUD Ebrahim
 * FAHRAOUI Zakaria
 */
final case class MowerState(
    start: Mower,
    instructions: List[Instructions.Value],
    end: Mower
) {}

object MowerState {
  implicit val mowerStateWrites: Writes[MowerState] = Json.writes[MowerState]
}
