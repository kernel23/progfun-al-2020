package entities

import entities.Parsers.Parser
import entities.Parsers.Parser.isAllDigit
import play.api.libs.json.{Json, OWrites}

import scala.annotation.tailrec
import scala.sys.exit

/**
 * Owners:
 * SAIDI Amina
 * OUADOUD Ebrahim
 * FAHRAOUI Zakaria
 */
object Coordinates {

  case class Coordinate(x: Int, y: Int) {

    val _coordinateX: Lens[Coordinate, Int] = new Lens[Coordinate, Int] {
      override def get: Coordinate => Int = _.x

      override def set: (Coordinate, Int) => Coordinate = {
        case (c, n) => c.copy(x = n)
      }
    }
    val _coordinateY: Lens[Coordinate, Int] = new Lens[Coordinate, Int] {
      override def get: Coordinate => Int = _.y

      override def set: (Coordinate, Int) => Coordinate = {
        case (c, n) => c.copy(y = n)
      }
    }

    override def toString: String =
      "Coordinate( x=" + x.toString + " , y=" + y.toString + " )"
  }

  object Coordinate {

    implicit val coordinateImplicitWrites: OWrites[Coordinate] =
      Json.writes[Coordinate]

    def listToCoordinate(numbers: List[Int]): Coordinate =
      numbers match {
        case Nil      => exit(1)
        case _ :: Nil => exit(1)
        case head :: tail =>
          new Coordinate(tail.headOption.getOrElse({ exit(1) }), head)
      }

    @tailrec
    def helpParseCoordinate(
        split: List[String],
        numbers: List[Int]
    ): Coordinate =
      split match {
        case Nil => listToCoordinate(numbers)
        case head :: tail =>
          if (head != " " && isAllDigit(head.toList))
            helpParseCoordinate(tail, head.toInt :: numbers)
          else helpParseCoordinate(tail, numbers)
      }

    implicit val parser: Parser[Coordinate] = (content: List[String]) =>
      content.headOption match {
        case None        => exit(1)
        case Some(value) => helpParseCoordinate(value.split(' ').toList, List())
      }
  }
}
