package entities

import entities.Coordinates.Coordinate
import play.api.libs.json.{Json, Writes}

/**
 * Owners:
 * SAIDI Amina
 * OUADOUD Ebrahim
 * FAHRAOUI Zakaria
 */
final case class Result(
    limit: Coordinate,
    mowers: List[MowerState]
)

object Result {
  implicit val writes: Writes[Result] = Json.writes[Result]
}

//  implicit val writes: Writes[Result] = Json.writes[Result]
//  Json.prettyPrint(Json.toJson(writes.toString))
