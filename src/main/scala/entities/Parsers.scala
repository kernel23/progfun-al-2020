package entities

import scala.annotation.tailrec

/**
 * Owners:
 * SAIDI Amina
 * OUADOUD Ebrahim
 * FAHRAOUI Zakaria
 */
object Parsers {

  trait Parser[A] {
    def parse(content: List[String]): A
  }

  object Parser {
    def apply[A](implicit parser: Parser[A]): Parser[A] = parser

    implicit def parse[A](implicit parser: Parser[A]): Parser[A] = parser

    @tailrec
    def isAllDigit(str: List[Char]): Boolean =
      str match {
        case Nil          => false
        case head :: Nil  => head.isDigit
        case head :: tail => head.isDigit && isAllDigit(tail)
      }

  }
}
