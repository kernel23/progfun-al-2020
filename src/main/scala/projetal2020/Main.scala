package projetal2020

import better.files.File
import entities.Coordinates.Coordinate
import entities.{MowerCommand, MowerState, Result}
import entities.Mowers.Mower

/**
 * Owners:
 * SAIDI Amina
 * OUADOUD Ebrahim
 * FAHRAOUI Zakaria
 */

object Main extends App {

  // Nous avons récupéré toutes les lignes du fichier
  val file: File = File("input.txt")
  val lines = file.lines.toList

  // Récupérations: les Instructions, les Mowers, et les Coordinates
  val coordinates = Coordinate.parser.parse(lines)
  val mowers = Mower.parser.parse(lines)

  // Pour tester les objets Mowers
  print("Mowers : ")
  println(mowers)

  println("===" * 40)
  // L'exécutions des MowersStates
  val mowerStates: List[MowerState] =
    MowerCommand.execute(mowers)

  // Pour tester les objets MowerStates
  print("MowerStates : ")
  println(mowerStates)
  // Nous avons récupère le result Mowers en JSON
  val result = new Result(coordinates, mowerStates)
  val outputFile: File = File("Result.json")

  //JSON
  outputFile
    .createIfNotExists()
    .appendLine(Result.writes.writes(result).toString())
}
